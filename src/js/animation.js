~ function () {
	'use strict';
	var $ = TweenMax,
		spriteSheet,
		ad = document.getElementById('mainContent'),
		bgExit = document.getElementById('bgExit'),	
		easing = Power3.easeInOut;

	window.init = function () {
		play();
		addListeners();
		
		spriteSheet = new spriteSheetAnim('mainAnimation',
				'img/spriteSheet.png',
				200, 200, 4000,
				1200, 120, false, 52);
		
	}

	function play() {

		var tl = new TimelineLite();
		tl.set('#mainContent', { force3D: true })
		tl.addLabel('frame')
		tl.add(function(){spriteSheet.ss_playRange(120,0)});
		tl.to('#mainAnimation',0.5, {opacity:0, ease:Power1.easeInOut},'frame+=6');
		tl.to(['#logo','#endCopy','.bgCircle'], 0.5,{ opacity: 1, ease:Power1.easeInOut }, 'frame+=6.1');

	}
	
	function addListeners() {
		bgExit.addEventListener('click', bgExitHandler);
	}

	function bgExitHandler(e) {
		Enabler.exit("Background Exit");
	}

}();

